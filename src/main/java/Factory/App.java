package Factory;

public class App {
	public static void main(String[] args) {
	ICarro hatch = FabricaDeCarro.fabricarCarro(Categoria.HATCH);
	ICarro sedan = FabricaDeCarro.fabricarCarro(Categoria.HATCH);
	ICarro tracker = FabricaDeCarro.fabricarCarro(Categoria.HATCH);
	
	System.out.println(hatch + ": " + hatch.getDescricao());
	System.out.println(sedan + ": " + sedan.getDescricao());
	System.out.println(tracker + ": " + tracker.getDescricao());
	}
}
