package Factory;

public abstract class FabricaDeCarro {
	public static ICarro fabricarCarro(Categoria categoria) {
		switch (categoria) {
		case HATCH:
			return new Onix();
		case SEDAN:
			return new Corolla();
		case SUV:
			return new Hilux();
		default:
			break;
		}
		return null;
	}
}
