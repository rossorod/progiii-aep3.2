package Observer;

public class CartaoDeCredito {
	private double totalDeDebito;
	private ICompra compra;

	public void comprar(double valor) {
		if (compra != null) {
			compra.notificar(valor);
		}
		this.totalDeDebito += valor;
	}

	public void ativarNotificacao(ICompra compra) {
		this.compra = compra;
	}
	
	public void desativarNotificacao() {
		this.compra = null;
	}

}
