package Observer;

import javax.swing.JOptionPane;

public class App {

	public static void main(String[] args) {
		CartaoDeCredito cartao = new CartaoDeCredito();
		
		cartao.comprar(158.99);
		
		cartao.ativarNotificacao(new ICompra() {
			public void notificar(double valor) {
				JOptionPane.showMessageDialog(null, "voce efetuou uma compra no Valor: " + valor);
			}
		});
		
		cartao.comprar(158.99);
		cartao.comprar(222.99);
		cartao.comprar(99.99);
		
		cartao.desativarNotificacao();
		cartao.ativarNotificacao(new ICompra() {
			public void notificar(double valor) {
				System.out.println("voce efetuou uma compra no Valor: " + valor);
			}			
		});
		
		cartao.comprar(99.99);
		cartao.comprar(99.99);
		cartao.desativarNotificacao();
		cartao.comprar(79.99);
	}
}
