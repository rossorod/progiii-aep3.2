package Observer;

public interface ICompra {
	void notificar(double valor);
}
