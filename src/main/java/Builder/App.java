package Builder;

public class App {
	public static void main(String[] args) {
		Pizza pizza = new Pizza.Builder(10)
        .queijo()
        .tomate()
        .bacon()
        .build();
	}
}
